<?php
//this whole thing isn't working...
function sdg_retrieve($atts) {
	$terms = 0;
	$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
	$paging = ""; //got undefined variable, so have to declare it here, likely out of scope due to loop
	while ( $terms != count( $atts[ 'terms' ] ) ) {
		$args = array(
			'post_type' 	 => 'attachment',
			'post_status' 	 => 'inherit', 
			'post_mime_type' => $atts[ 'post_mime_type' ],
			'posts_per_page' => $atts[ 'posts_per_page' ],
			'paged'			 => $paged,
			'order'			 => 'ASC',
			'orderby'		 => 'terms', //may have to nest the array
			'tax_query' 	 => array(
				array(
					'taxonomy' => $atts[ 'taxonomy' ],
					'field'	   => 'slug',
					'terms'    => $atts[ 'terms' ][ $terms ], //terms may need to be an array
					),
			)
		);
		$query = new WP_Query( $args );
		
		$return_string = '<h1>'. $atts[ 'terms'][ $terms ] .'</h1>';
		$return_string .= '<ul>';
		$terms++;
		if ( $query->have_posts() ) {
			//get mime type and add that to this and yeah change the shortcode and parameterize it...

			while ( $query->have_posts() ) {
				$query->the_post();

				$mime_type = sdg_get_mime_type( get_attached_file( get_the_ID() ) );

				if ( $mime_type == "txt" ) {
					$file = file_get_contents( get_attached_file( get_the_ID() ) );
					
					$return_string .= '<li class="page-'.$atts[ 'icon_size' ].'"><a href="'.$file.'" target="_blank">'.get_the_title().'</a></li>';
				}else {
					$return_string .= '<li class="'.$mime_type.'-'.$atts[ 'icon_size' ].'"><a href="'.wp_get_attachment_url().'" target="_blank">'.get_the_title().'</a></li>';
				}
			}
			/*
			$paging = paginate_links( array(
				'base' 		=> str_replace( 999999999, '%#%', esc_url( get_pagenum_link( 999999999 ) ) ),
				'total' 	   => $query->max_num_pages,
				'current'      => max( 1, get_query_var( 'paged' ) ),
				'format'       => '?paged=%#%',
				'show_all'     => false,
				'type'		   => 'plain',
				'end_size' 	   => 2,
				'mid_size'	   => 1,
				'prev_next'    => true,
				'prev_text'    => '<<', //the next page symbol
				'next_text'    => '>>', //the previous page symbol
				'add_args'     => false,
				'add_fragment' => '',
			) );*/	
		}else {
			$return_string .= '<li>no documents found</li>';
		}
		$return_string .= '</ul>';
		echo $return_string;
		
		
		//return $return_string;
	}
	wp_pagenavi();
	//echo $paging;
	wp_reset_postdata();
}
	

function sdg_get_mime_type($file) {
	return wp_check_filetype($file)['ext'];
}
?>