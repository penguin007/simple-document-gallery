<?php
/*
Plugin Name: Simple Document Gallery
Description: Displays documents on WordPress Websites in conjuction with Enhanced Media Library
Version: 0.1
Author: Matthew Mills
License: GPLv3

Simple Document Library is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
any later version.
 
Simple Document Library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.
 
You should have received a copy of the GNU General Public License
along with Simple Document Library. If not, see https://www.gnu.org/licenses/gpl-3.0.en.html.
*/

defined( 'ABSPATH' ) or die ( 'stay out');

define( 'SDG_PATH', plugin_dir_path( __FILE__ ) ); //this might not be working

//initialize the shortcode
function sdg_shortcodes_init() {
	
	
	function sdg_create( $atts = [], $content = null ) {
		require_once( 'includes/sdg-functions.php' );
		
		//normalize attributes
		$atts = array_change_key_case((array)$atts, CASE_LOWER);
		
		$atts = shortcode_atts(
			array (
				'terms' 		 => array(), //this is required yet an empty parameter...got to account for it
				'taxonomy' 		 => 'media_category', //eml default
				'post_mime_type' => '*',
				'icon_size'		 => '16px',
				'posts_per_page' => '6', //CHANGE THIS IN PRODUCTION
				), $atts, 'sdg-create'
		);
		$atts[ 'terms' ] = explode( ',', $atts[ 'terms' ] ); //explode delimited list into an index array
		//print_r($atts);
		wp_enqueue_style( 'sdg-mimes.min.css', plugin_dir_url( __FILE__ ) . '/includes/sdg-mimes.min.css' );
		
		return sdg_retrieve($atts); //calls the function
	}
	
	if ( !shortcode_exists( 'sdg-create' ) ) {
		add_shortcode( 'sdg-create', 'sdg_create' );
	}else {
		error_log( 'The shortcode \'sdg-create\' for this plugin is already present in the WordPress installation, please ensure that other plugins are not interfering with the shortcode', 0);
	}
}
add_action( 'init', 'sdg_shortcodes_init' );
?>