=== Simple Document Gallery ===
Contributors: Matthew Mills
Tags: Document Gallery, Enhanced Media Library
Requires at least: 4.5
Tested up to: 4.98
Requires PHP: 5.6
License: GPLv3
License URI: http://www.gnu.org/licenses/gpl-3.0.html

A plugin that utilizes Enhanced Media Library\'s to display a gallery of document files as opposed to images. Note that Enhanced Media Gallery must be installed